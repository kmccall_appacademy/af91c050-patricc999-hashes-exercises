# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  lengths = {}
  str.split(' ').each { |word| lengths[word] = word.length }
  lengths
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  sorted_hash = hash.sort_by { |_key, value| value }
  sorted_hash.last[0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |item, count|
    older[item] = count
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  words_hash = {}
  letters = word.chars.uniq
  letters.each do |letter|
    words_hash[letter] = word.chars.count(letter)
  end
  words_hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hash = {}
  arr.each { |element| hash[element] = true }
  hash.keys.to_a
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  hash = { :even => 0, :odd => 0 }
  numbers.each do |num|
    if num.even?
      hash[:even] = hash[:even] + 1
    else
      hash[:odd] = hash[:odd] + 1
    end
  end
  hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowel_hash = {}
  string.chars.each do |letter|
    vowel_hash[letter] = string.chars.count(letter)
  end
  sorted = vowel_hash.sort_by { |letter, number| number }
  sorted[-1][0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  combo_array = []
  valid_birthdays = students.reject { |key, value| value < 7 }
  valid_birthdays.keys.each_index do |idx1|
    ((idx1 + 1)...valid_birthdays.keys.length).each do |idx2|
      combo_array.push([valid_birthdays.keys[idx1], valid_birthdays.keys[idx2]])
    end
  end
  combo_array
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  specimens_counter = Hash.new(0)
  specimens.each do |species|
    specimens_counter[species] += 1
  end
  number_of_species = specimens_counter.keys.length
  smallest_population_size = specimens_counter.values.sort.first
  largest_population_size = specimens_counter.values.sort.last
  number_of_species ** 2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_count = character_count(normal_sign)
  vandalized_counter = character_count(vandalized_sign)
  vandalized_counter.each do |char, count|
    if normal_count[char] < count
      return false
    end
  end
  true
end

def character_count(str)
  character_counter = Hash.new(0)
  str.chars.each do |char|
    character_counter[char.downcase] += 1
  end
  character_counter
end
